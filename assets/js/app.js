const asyncLocalStorage = {
  setItem: function(key, value) {
    return Promise.resolve().then(function() {
      localStorage.setItem(key, value);
    });
  },
  getItem: function(key) {
    return Promise.resolve().then(function() {
      return localStorage.getItem(key);
    });
  },
  clearAll: function() {
    return Promise.resolve().then(function() {
      return localStorage.clear();
    });
  },
  removeItem: function(key) {
    return Promise.resolve().then(function() {
      return localStorage.removeItem(key);
    });
  }
};

function logIn() {
  asyncLocalStorage
    .removeItem("userData")
    .then(() => {
      window.location.href = "login.html";
    })
    .catch(e => console.error(e));
}

function useLoo() {
  asyncLocalStorage
    .getItem("hasLoggedInBefore")
    .then(result => {
      result = JSON.parse(result);
      result.times = 0;
      asyncLocalStorage
        .setItem("hasLoggedInBefore", JSON.stringify(result))
        .then(() => {
          window.location.href = "../index.html";
        })
        .catch(e => console.error(e));
    })
    .catch(e => console.error(e));
}

// function home() {
//   window.location.href = "../index.html";
// }

if (document.getElementById("home")) {
  asyncLocalStorage.getItem("hasLoggedInBefore").then(data => {
    data = JSON.parse(data);
    data.times += 1;

    asyncLocalStorage
      .setItem("hasLoggedInBefore", JSON.stringify(data))
      .then(() => {
        if (data.state == true && data.times <= 1 && data.type == "normal") {
          axios
            .get("https://parseapi.back4app.com/classes/Cubicle?include=user", {
              headers: {
                "X-Parse-Application-Id":
                  "LrGNhN6yK7D9xDLPDDGmKdcSgSryf3YmKK2A6vxb",
                "X-Parse-REST-API-Key":
                  "gos3PDHaOOHBYpKXPxO6CTRMiLxjSZDN518T42Fa"
              }
            })
            .then(data => {
              console.log(data.data.results.length);

              if (data.data.results.length > 0) {
                const result = data.data.results;
                console.log(`There are ${result.length} cubicles available.`);

                // Check cubicles
                for (var i = 0; i < result.length; i++) {
                  if (typeof result[i].user === "undefined") {
                    console.log(`Cubicle ${i + 1} is empty.`);
                  } else {
                    $("#general").append(`<div class="userProfile">
                          <div class="avatar animated infinite pulse">
                            <img
                              src="${result[i].user.avatar}"
                              alt=""
                            />
                          </div>
                          <p>${result[i].user.username}</p>
                        </div>`);

                    if (result[i].number == 1) {
                      $("#stall1User").append(`<div class="userProfile">
                          <div class="avatar animated infinite pulse">
                            <img
                              src="${result[i].user.avatar}"
                              alt=""
                            />
                          </div>
                          <p>${result[i].user.username}</p>
                        </div>`);

                      $("#useStallBtn1").addClass("disabled");
                    } else if (result[i].number == 2) {
                      $("#stall2User").append(`<div class="userProfile">
                          <div class="avatar animated infinite pulse">
                            <img
                              src="${result[i].user.avatar}"
                              alt=""
                            />
                          </div>
                          <p>${result[i].user.username}</p>
                        </div>`);

                      $("#useStallBtn2").addClass("disabled");
                    }
                  }
                }
              }
            })
            .catch(e => {
              console.error(
                JSON.stringify(e.response.data.error, undefined, 2)
              );
              alert(e.response.data.error);
            });

          let scanner = new Instascan.Scanner({
            video: document.getElementById("qrPreview"),
            mirror: false
          });

          scanner.addListener("scan", function(content) {
            if(content == "0b2d43b221ae84d1d9288a951a914d1e"){
                $("#stallBtn").click();
            }
            else{
                alert("Invalid QR code.")
            }  
          });

          Instascan.Camera.getCameras()
            .then(function(cameras) {
              if (cameras.length > 0) {
                scanner.start(cameras[1]);
              } else {
                console.error("No cameras found.");
              }
            })
            .catch(function(e) {
              console.error(e);
            });

          setTimeout(() => {
            scanner
              .stop()
              .then(() => {
                console.log("Camera has been stopped.");
              })
              .catch(e => {
                console.error(e);
              });
          }, 50000);

          function confirmStall(stallID) {
            bootbox.confirm({
              backdrop: true,
              centerVertical: true,
              message:
                stallID == "ASRLB8jP2Y"
                  ? "Just making sure you're going to exactly Cubicle 1."
                  : "Just making sure you're going to exactly Cubicle 2.",
              buttons: {
                confirm: {
                  label: "Yes that's it.",
                  className: "btn-success",
                  callback: function(result) {}
                },
                cancel: {
                  label: "Let me be sure.",
                  className: "btn-danger",
                  callback: function(result) {}
                }
              },
              callback: function(result) {
                if (result) {
                  asyncLocalStorage.getItem("userData").then(result => {
                    result = JSON.parse(result);
                    axios
                      .put(
                        `https://parseapi.back4app.com/classes/Cubicle/${stallID}`,
                        {
                          user: {
                            __type: "Pointer",
                            className: "_User",
                            objectId: `${result.objectId}`
                          }
                        },
                        {
                          headers: {
                            "X-Parse-Application-Id":
                              "LrGNhN6yK7D9xDLPDDGmKdcSgSryf3YmKK2A6vxb",
                            "X-Parse-REST-API-Key":
                              "gos3PDHaOOHBYpKXPxO6CTRMiLxjSZDN518T42Fa",
                            "Content-Type": "application/json"
                          }
                        }
                      )
                      .then(data => {
                        if (data.status == 200) {
                          asyncLocalStorage
                            .setItem("userStallChoice", stallID)
                            .then(result => {
                              console.log("Cubicle ID stored.");
                              window.location.href = "pages/userInStall.html";
                            });
                        }
                      })
                      .catch(e => {
                        console.error(e);
                      });
                  });
                }
                //    alert("This was logged in the callback: " + result);
              }
            });
          }

          $("#useStallBtn1").click(() => {
            confirmStall("ASRLB8jP2Y");
          });

          $("#useStallBtn2").click(() => {
            confirmStall("1O1Tlkjq0f");
          });
        } else {
          window.location.href = "pages/welcome.html";
        }
      })
      .catch(e => {
        console.error(e);
      });
  });
}

if (document.getElementById("confirmPage")) {
  asyncLocalStorage.getItem("userData").then(result => {
    console.log(result);
    result = JSON.parse(result);
    $("#stallUser").prepend(`<div class="userProfile">
        <div class="avatar animated infinite pulse">
          <img
            src="${result.avatar}"
            alt=""
          />
        </div>
        <p class = "mainName">${result.username}</p>
        <p class = "stallStatus">In stall.</p>
        <p class = "secMessage">Other users will be notified that you are in Stall 1.</p>
      </div>`);
  });
  $("#doneBtn").click(() => {
    asyncLocalStorage
      .getItem("userStallChoice")
      .then(result => {
        axios
          .put(
            `https://parseapi.back4app.com/classes/Cubicle/${result}`,
            {
              user: {
                __type: "Pointer",
                className: "_User",
                objectId: ``
              }
            },
            {
              headers: {
                "X-Parse-Application-Id":
                  "LrGNhN6yK7D9xDLPDDGmKdcSgSryf3YmKK2A6vxb",
                "X-Parse-REST-API-Key":
                  "gos3PDHaOOHBYpKXPxO6CTRMiLxjSZDN518T42Fa",
                "Content-Type": "application/json"
              }
            }
          )
          .then(data => {
            console.log(data);
            asyncLocalStorage
              .getItem("hasLoggedInBefore")
              .then(confirmData => {
                confirmData = JSON.parse(confirmData);
                confirmData.times = 1;
                asyncLocalStorage
                  .setItem("hasLoggedInBefore", JSON.stringify(confirmData))
                  .then(() => {
                    window.location.href = "../index.html";
                  })
                  .catch(e => console.error(e));
              })
              .catch(e => console.error(e));
          })
          .catch(e => console.error(e));
      })
      .catch(e => console.error(e));
  });
}

if (document.getElementById("loginPage")) {
  $("#loginBtn").click(() => {
    const username = $("#username").val();
    const password = $("#password").val();
    axios
      .get(
        `https://parseapi.back4app.com/login?username=${username}&password=${password}`,
        {
          headers: {
            "X-Parse-Application-Id":
              "LrGNhN6yK7D9xDLPDDGmKdcSgSryf3YmKK2A6vxb",
            "X-Parse-REST-API-Key": "gos3PDHaOOHBYpKXPxO6CTRMiLxjSZDN518T42Fa",
            "X-Parse-Revocable-Session": "1"
          }
        }
      )
      .then(data => {
        console.log(data.data);
        asyncLocalStorage
          .setItem("userData", JSON.stringify(data.data))
          .then(result => {
            asyncLocalStorage
              .setItem(
                "hasLoggedInBefore",
                JSON.stringify({ state: true, times: 0, type: "normal" })
              )
              .then(() => {
                window.location.href = "../index.html";
              });
          });
      })
      .catch(e => {
        console.error(JSON.stringify(e.response.data.error, undefined, 2));
        alert(e.response.data.error);
      });
  });
}

if (document.getElementById("welcomeUser")) {
  asyncLocalStorage
    .getItem("userData")
    .then(data => {
      if (data) {
        data = JSON.parse(data);
        $("#welcomeUser").append(`<section class="userInStall">
    <p class = "stallStatus">Welcome back</p>
    <div class="userProfile">
    <div class="avatar animated infinite pulse">
      <img
        src="${data.avatar}"
        alt=""
      />
    </div>
    <p class = "mainName">${data.username}</p>
    </div>
    <button id = "doneBtn" type="button" class="btn btn-primary btn-lg btn-block" style = "margin-top : 1rem" onClick = "useLoo()">Use Loo</button>
    
    <p class = "secMessage" style = "margin-top : 2rem">Not User?</p>
    <button type="button" class="btn btn-secondary btn-sm" id = "switchUser" onClick = "logIn()">Switch User</button>
    </section>`);
      }
    })
    .catch(e => console.error(e));
}
